//
//  Created by Michał Mizera on 08.10.2013.
//


#import <Foundation/Foundation.h>


@interface AMCalibrationView : UIView
@property(nonatomic, strong) UIImageView *cameraImage;
@property(nonatomic, strong) UIButton *backButton;
@property(nonatomic, strong) UIButton *saveButton;
@property(nonatomic, strong) UIButton *acceptButton;
@property(nonatomic, strong) UIButton *rejectButton;
@property(nonatomic, strong) UIButton *finishButton;
@property(nonatomic, strong) UILabel *calibrationDataLabel;
@property(nonatomic, strong) UIProgressView *progressBar;
@end
