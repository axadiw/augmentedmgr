//
//  Created by Michał Mizera on 08.10.2013.
//


#import <Objection/Objection.h>
#import "AMCalibrationViewController.h"
#import "AMCalibrationView.h"
#import "AMArEngine.h"
#import "UIImage+OpenCV.h"
#import "AMCalibrator.h"
#import "AMUserDefaultsManager.h"
#import "NSString+Matrix.h"

@interface AMCalibrationViewController ()
@property(nonatomic, strong) AMArEngine *arEngine;
@property(nonatomic, strong) AMCalibrator *calibrator;
@property(nonatomic, strong) AMUserDefaultsManager *userDefaultsManager;
@end


@implementation AMCalibrationViewController {
    BOOL _finishedGatheringImages;
}

objection_requires_sel(@selector(arEngine), @selector(calibrator), @selector(userDefaultsManager))

- (void)awakeFromObjection {
    [super awakeFromObjection];

    _finishedGatheringImages = NO;
}


- (AMCalibrationView *)castView {
    return (AMCalibrationView *) self.view;
}

- (void)loadView {
    self.view = [[AMCalibrationView alloc] initWithFrame:CGRectZero];

    [self.castView.saveButton addTarget:self action:@selector(saveButtonTapped)
                       forControlEvents:UIControlEventTouchUpInside];
    [self.castView.finishButton addTarget:self action:@selector(finishButtonTapped)
                         forControlEvents:UIControlEventTouchUpInside];

    [self.castView.acceptButton addTarget:self action:@selector(acceptButtonButtonTapped)
                         forControlEvents:UIControlEventTouchUpInside];
    [self.castView.rejectButton addTarget:self action:@selector(rejectButtonTapped)
                         forControlEvents:UIControlEventTouchUpInside];


    [self.castView.backButton addTarget:self action:@selector(backButtonTapped)
                       forControlEvents:UIControlEventTouchUpInside];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self updateCalibrationDataLabel];
    [_arEngine addObserver:self];
    [_calibrator startCalibration];
    _calibrator.delegate = self;
    _arEngine.mode = AMArEngineModeCalibrating;
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [_arEngine removeObserver:self];
    _arEngine.mode = AMArEngineModeDisabled;
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)nextFrameReady {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!_finishedGatheringImages) {
            self.castView.cameraImage.image = [UIImage imageWithCVMat:*(_arEngine.image)];
        }
    });

}

- (void)updateCalibrationDataLabel {

    NSString *cameraMatrixString = [NSString stringFromMat:[_arEngine currentCameraMatrix]];
    cv::Mat coeffs = _userDefaultsManager.distCoeffs;
    NSString *coeffsString = [NSString stringFromMat:coeffs];

    NSString *calibrationData = [NSString stringWithFormat:@"Camera Matrix:\n%@ Coeffs:\n%@", cameraMatrixString,
                                                           coeffsString];

    self.castView.calibrationDataLabel.text = calibrationData;
    [self.castView setNeedsLayout];
}

#pragma mark -
#pragma mark Button actions

- (void)backButtonTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)rejectButtonTapped {
    [self loadNextProcessedImageAndEndCalibrationIfNeeded];
}

- (void)acceptButtonButtonTapped {
    [_calibrator acceptCurrentProcessedImage];

    [self loadNextProcessedImageAndEndCalibrationIfNeeded];
}

- (void)loadNextProcessedImageAndEndCalibrationIfNeeded {
    UIImage *nextImage = _calibrator.loadNextProcessedImage;
    if (nextImage) {
        self.castView.cameraImage.image = nextImage;
    }
    else {
        self.castView.acceptButton.hidden = YES;
        self.castView.rejectButton.hidden = YES;
        self.castView.calibrationDataLabel.text = [NSString stringWithFormat:@"%lu%@",_calibrator.approvedCorners->size(), self.castView.calibrationDataLabel.text];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [_calibrator endCalibration];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:YES completion:nil];
            });
        });
    }
}

- (void)finishButtonTapped {
    _finishedGatheringImages = YES;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [_calibrator processSavedImages];

        dispatch_async(dispatch_get_main_queue(), ^{
            self.castView.acceptButton.hidden = NO;
            self.castView.rejectButton.hidden = NO;

            UIImage *firstImage = [_calibrator loadNextProcessedImage];

            if (firstImage) {
                self.castView.cameraImage.image = firstImage;
            }
            else {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        });

    });
}


- (void)saveButtonTapped {
    [_calibrator saveRawImage:self.castView.cameraImage.image];
}

#pragma mark -
#pragma mark AMCalibratorDelegate

- (void)updateProgressBarWithValue:(CGFloat)progress {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.castView.progressBar setProgress:progress animated:YES];
    });
}


@end
