//
//  Created by Michał Mizera on 08.10.2013.
//


#import <PLVisualAttributeConstraints/NSLayoutConstraint+PLVisualAttributeConstraints.h>
#import "AMCalibrationView.h"


@implementation AMCalibrationView {
    UILabel *_calibrationDataLabel;
    UIView *_bottomButtonsContainerView;
    UIView *_middleButtonsContainerView;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _cameraImage = [[UIImageView alloc] init];
        _cameraImage.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_cameraImage];

        _bottomButtonsContainerView = [[UIView alloc] initWithFrame:CGRectZero];
        _bottomButtonsContainerView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_bottomButtonsContainerView];

        _middleButtonsContainerView = [[UIView alloc] initWithFrame:CGRectZero];
        _middleButtonsContainerView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_middleButtonsContainerView];

        _saveButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _saveButton.translatesAutoresizingMaskIntoConstraints = NO;
        _saveButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        [_saveButton setTitle:@"Zapisz" forState:UIControlStateNormal];
        [_bottomButtonsContainerView addSubview:_saveButton];

        _finishButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _finishButton.translatesAutoresizingMaskIntoConstraints = NO;
        _finishButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        [_finishButton setTitle:@"Zakończ kalibrację" forState:UIControlStateNormal];
        [_bottomButtonsContainerView addSubview:_finishButton];

        _acceptButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _acceptButton.translatesAutoresizingMaskIntoConstraints = NO;
        _acceptButton.titleLabel.font = [UIFont boldSystemFontOfSize:40];
        [_acceptButton setTitle:@"Zaakceptuj" forState:UIControlStateNormal];
        _acceptButton.hidden = YES;
        [_middleButtonsContainerView addSubview:_acceptButton];

        _rejectButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _rejectButton.translatesAutoresizingMaskIntoConstraints = NO;
        _rejectButton.titleLabel.font = [UIFont boldSystemFontOfSize:40];
        [_rejectButton setTitle:@"Odrzuć" forState:UIControlStateNormal];
        _rejectButton.hidden = YES;
        [_middleButtonsContainerView addSubview:_rejectButton];

        _backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _backButton.translatesAutoresizingMaskIntoConstraints = NO;
        _backButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        [_backButton setTitle:@"Wróć" forState:UIControlStateNormal];
        [self addSubview:_backButton];

        _progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        _progressBar.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_progressBar];

        _calibrationDataLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _calibrationDataLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _calibrationDataLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        _calibrationDataLabel.textColor = [UIColor whiteColor];
        _calibrationDataLabel.font = [UIFont systemFontOfSize:7];
        _calibrationDataLabel.numberOfLines = 0;
        [self addSubview:_calibrationDataLabel];


        NSDictionary *views = @{
                @"self" : self,
                @"camera" : _cameraImage,
                @"backButton" : _backButton,
                @"saveButton" : _saveButton,
                @"finishButton" : _finishButton,
                @"acceptButton" : _acceptButton,
                @"rejectButton" : _rejectButton,
                @"progressBar" : _progressBar,
                @"calibrationDataLabel" : _calibrationDataLabel,
                @"bottomButtonsContainer" : _bottomButtonsContainerView,
                @"middleButtonsContainer" : _middleButtonsContainerView,
        };

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[camera]|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[camera]|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[backButton]"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[calibrationDataLabel]"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[backButton]"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[calibrationDataLabel]|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[progressBar]-|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[middleButtonsContainer]-[progressBar]-[bottomButtonsContainer]-|"
                                                                     options:NSLayoutFormatAlignAllCenterX
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[rejectButton]-50-[acceptButton]|"
                                                                     options:NSLayoutFormatAlignAllCenterY
                                                                     metrics:nil
                                                                       views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[saveButton]-50-[finishButton]|"
                                                                     options:NSLayoutFormatAlignAllCenterY
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rejectButton]|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[saveButton]|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];
    }

    return self;
}

@end
