//
//  Created by Michał Mizera on 19.04.2013.
//


#import <Foundation/Foundation.h>

@protocol AMMarkersMapViewDelegate;
@class AMMarkersMap;


@interface AMMarkersMapView : UIView

@property (nonatomic) id<AMMarkersMapViewDelegate> delegate;

- (id)initWithFrame:(CGRect)frame markersMap:(AMMarkersMap *)aMarkersMap;

- (void)resetAllMarkers;

- (void)updateWithMarkersMap:(AMMarkersMap *)_markersMap centerMarkerId:(NSInteger)_centerMarkerId;
@end
