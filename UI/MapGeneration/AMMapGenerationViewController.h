//
//  Created by Michał Mizera on 08.10.2013.
//


#import <Foundation/Foundation.h>
#import <NinevehGL/NinevehGL.h>
#import "AMMarkersMapViewDelegate.h"
#import "AMArEngineDelegate.h"


@interface AMMapGenerationViewController : UIViewController <AMMarkersMapViewDelegate, AMArEngineDelegate>
@end
