//
//  Created by Michał Mizera on 08.10.2013.
//


#import <NinevehGL/NinevehGL.h>
#import "AMMapGenerationView.h"
#import "AMMarkersMapView.h"
#import "AMMarkersMap.h"

CGFloat AMMapGenerationViewMaxZoom = 5.0f;

@implementation AMMapGenerationView {
    UIButton *_backButton;
    UIScrollView *_mapViewScrollView;
}

- (id)initWithFrame:(CGRect)frame markersMap:(AMMarkersMap *)markersMap {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor orangeColor];

        _cameraImage = [[UIImageView alloc] init];
        _cameraImage.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_cameraImage];

        _undistortedImage = [[UIImageView alloc] init];
        _undistortedImage.contentMode = UIViewContentModeScaleAspectFit;
        _undistortedImage.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_undistortedImage];

        self.canonicalMarkerImage = [[UIImageView alloc] init];
        [self addSubview:self.canonicalMarkerImage];

        self.lastErrorLabel = [[UILabel alloc] init];
        _lastErrorLabel.font = [UIFont systemFontOfSize:20];
        _lastErrorLabel.backgroundColor = [UIColor clearColor];
        _lastErrorLabel.textColor = [UIColor greenColor];
        _lastErrorLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_lastErrorLabel];

        self.debugLabel = [[UILabel alloc] init];
        self.debugLabel.backgroundColor = [UIColor clearColor];
        self.debugLabel.numberOfLines = 0;
        self.debugLabel.shadowOffset = CGSizeMake(0, 1);
        self.debugLabel.shadowColor = [UIColor blackColor];
        self.debugLabel.textColor = [UIColor colorWithRed:0.15f green:0.61f blue:1.00f alpha:1.00f];
        self.debugLabel.font = [UIFont boldSystemFontOfSize:12];
        [self addSubview:self.debugLabel];

        _mapViewScrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _mapViewScrollView.delegate = self;
        _mapViewScrollView.minimumZoomScale = 1.0f / AMMapGenerationViewMaxZoom;
        _mapViewScrollView.maximumZoomScale = 1;
        _mapViewScrollView.zoomScale = 1.0f;
        [self addSubview:_mapViewScrollView];

        _markersMapView = [[AMMarkersMapView alloc] initWithFrame:CGRectZero markersMap:markersMap];
        [_mapViewScrollView addSubview:_markersMapView];

        _resetButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _resetButton.translatesAutoresizingMaskIntoConstraints = NO;
        _resetButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        [_resetButton setTitle:@"Reset" forState:UIControlStateNormal];
        [self addSubview:_resetButton];

        _backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _backButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        [_backButton setTitle:@"Wróć" forState:UIControlStateNormal];
        [self addSubview:_backButton];

        NSDictionary *views = @{
                @"undistorted" : _undistortedImage,
                @"reset" : _resetButton,
        };

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[undistorted(213)]|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[undistorted(160)]|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[reset]-|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[reset]"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];

    }

    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _cameraImage.frame = [[UIScreen mainScreen] bounds];

    self.debugLabel.frame = [[UIScreen mainScreen] bounds];
    self.canonicalMarkerImage.frame = CGRectMake(90, 50, 70, 70);
    _lastErrorLabel.frame = self.canonicalMarkerImage.frame;

    _mapViewScrollView.frame = self.bounds;

    CGRect bigFrame = self.bounds;

    bigFrame.size.height *= AMMapGenerationViewMaxZoom;
    bigFrame.size.width *= AMMapGenerationViewMaxZoom;
    _markersMapView.frame = bigFrame;
    _mapViewScrollView.contentSize = bigFrame.size;

    _backButton.frame = CGRectZero;
    CGRect backButtonFrame = _backButton.frame;
    backButtonFrame.origin.y = 50;
    _backButton.frame = backButtonFrame;
    [_backButton sizeToFit];
}

- (void)scrollToCenter {
    [self setNeedsLayout];
    [self layoutIfNeeded];

    CGRect centerRect = self.bounds;
    CGSize scrollViewSize = _markersMapView.frame.size;
    centerRect.origin.x = (scrollViewSize.width - centerRect.size.width) / 2.0f;
    centerRect.origin.y = (scrollViewSize.height - centerRect.size.height) / 2.0f;

    [_mapViewScrollView scrollRectToVisible:centerRect animated:NO];
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _markersMapView;
}


@end
