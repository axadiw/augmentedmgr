//
//  Created by Michał Mizera on 16.11.2013.
//


#import <Foundation/Foundation.h>
#import "AMArEngineDelegate.h"
#import "threeMF.h"


@interface AMGameplayViewController : UIViewController <AMArEngineDelegate, TMFConnectorDelegate>
- (void)didAddSubscription;

- (void)didRemoveSubscription;
@end
