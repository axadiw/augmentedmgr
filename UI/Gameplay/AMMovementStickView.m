#import "AMMovementStickView.h"

@implementation AMMovementStickView {
    CGFloat _radius;
    UIImageView *_viewIn, *_viewOut;
}

- (id)init {
    if ((self = [super init])) {
        _viewIn = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"move_control_in"]];
        _viewOut = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"move_control_out"]];

        self.frame = _viewOut.frame;
        _viewIn.center = _viewOut.center;
        _radius = _viewOut.frame.size.width * 0.5f;

        [self addSubview:_viewIn];
        [self addSubview:_viewOut];
    }

    return self;
}

- (void)setInnerPosition:(CGPoint)value {
    CGPoint newCenter = CGPointMake(value.x - self.frame.origin.x, value.y - self.frame.origin.y);
    CGPoint origin = _viewOut.center;

    if (distanceBetweenPoints(newCenter, origin) > _radius) {
        CGFloat angle = atan2f(newCenter.y - origin.y, newCenter.x - origin.x);
        newCenter = CGPointMake(origin.x + _radius * cosf(angle), origin.y + _radius * sinf(angle));
    }

    _viewIn.center = newCenter;
}


- (void)setOuterPosition:(CGPoint)value {
    if (_touch == nil) {
        self.center = value;
    }
}

- (void)setTouch:(UITouch *)value {
    if (_touch == nil || value == nil) {
        _touch = value;
        _viewIn.center = _viewOut.center;
    }
}

- (CGPoint)movement {
    CGPoint moveVector = CGPointZero;

    if (_touch != nil) {
        CGPoint smallCircle = _viewIn.center;
        CGPoint bigCircle = _viewOut.center;
        float scalarDistance = distanceBetweenPoints(smallCircle, bigCircle) / _radius;
        float angle;

        angle = atan2f(smallCircle.y - bigCircle.y, smallCircle.x - bigCircle.x);

        CGFloat xAcceleration = -sinf(angle) * scalarDistance;
        CGFloat yAcceleration = cosf(angle) * scalarDistance;

        moveVector = CGPointMake(xAcceleration, yAcceleration);
    }

    return moveVector;
}
@end
