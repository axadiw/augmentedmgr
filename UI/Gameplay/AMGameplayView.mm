//
//  Created by Michał Mizera on 16.11.2013.
//


#import <Objection/JSObjection.h>
#import <PLVisualAttributeConstraints/NSLayoutConstraint+PLVisualAttributeConstraints.h>
#import "AMGameplayView.h"
#import "AMMarkersMap.h"
#import "AMGameplay3DOverlayView.h"


@implementation AMGameplayView

- (id)initWithFrame:(CGRect)frame andMarkersMap:(AMMarkersMap *)markersMap {
    self = [super initWithFrame:frame];
    if (self) {
        _cameraImage = [[UIImageView alloc] init];
        _cameraImage.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_cameraImage];

        _overlay3d = [JSObjection defaultInjector][[AMGameplay3DOverlayView class]];
        _overlay3d.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_overlay3d];
        
        _onlineText = [[UILabel alloc] initWithFrame:CGRectZero];
        _onlineText.transform = CGAffineTransformMakeRotation(nglDegreesToRadians(90));
        _onlineText.font = [UIFont systemFontOfSize:15];
        _onlineText.translatesAutoresizingMaskIntoConstraints = NO;
        _onlineText.textColor = [UIColor greenColor];
        _onlineText.backgroundColor = [UIColor clearColor];
        _onlineText.text = @"Online";
        _onlineText.hidden = NO;
        [self addSubview:_onlineText];

        _distanceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _distanceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _distanceLabel.transform = CGAffineTransformMakeRotation(nglDegreesToRadians(90));
        _distanceLabel.backgroundColor = [UIColor clearColor];
        _distanceLabel.textColor = [UIColor whiteColor];
        _distanceLabel.font = [UIFont boldSystemFontOfSize:15];
        [self addSubview:_distanceLabel];

        _backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _backButton.translatesAutoresizingMaskIntoConstraints = NO;
        _backButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        [_backButton setTitle:@"Wróć" forState:UIControlStateNormal];
        _backButton.transform = CGAffineTransformMakeRotation(nglDegreesToRadians(90));
        [self addSubview:_backButton];

        _noMapFoundLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _noMapFoundLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _noMapFoundLabel.text = @"Nie wykryto mapy!";
        _noMapFoundLabel.font = [UIFont boldSystemFontOfSize:30];
        [self addSubview:_noMapFoundLabel];

        [self setupConstraints];


    }

    return self;
}

- (void)setupConstraints {
    [self removeConstraints:self.constraints];

    NSDictionary *views = @{
            @"self" : self,
            @"camera" : _cameraImage,
            @"overlay" : _overlay3d,
            @"backButton" : _backButton,
            @"noMapFound" : _noMapFoundLabel,
            @"onlineLabel" : _onlineText,
            @"distanceLabel" : _distanceLabel,
    };

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[camera]|"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[camera]|"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[overlay]|"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[overlay]|"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:views]];

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50-[backButton]"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[backButton]-5-|"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:views]];


    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[distanceLabel]-|"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[distanceLabel]|"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:views]];



    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50-[onlineLabel]"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[onlineLabel]"
                                                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                                                 metrics:nil
                                                                   views:views]];


    [self addConstraints:[NSLayoutConstraint attributeConstraintsWithVisualFormatsArray:@[
            @"noMapFound.centerX == self.centerX",
            @"noMapFound.centerY == self.centerY",
    ]
                                                                                  views:views]];
}

- (void)setDistance:(CGFloat)distance {
    _distanceLabel.text = [NSString stringWithFormat:@"Odległość od markera centralego: %.2f m", distance];
}

@end
