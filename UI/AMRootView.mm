//
//  Created by Michał Mizera on 17.04.2013.
//


#import "PLVisualAttributeConstraints/NSLayoutConstraint+PLVisualAttributeConstraints.h"
#import "AMRootView.h"


@implementation AMRootView {
    UIView *_buttonsContainer;
}


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor orangeColor];

        _cameraImage = [[UIImageView alloc] init];
        _cameraImage.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_cameraImage];

        _buttonsContainer = [[UIView alloc] initWithFrame:CGRectZero];
        _buttonsContainer.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_buttonsContainer];

        _previewMapButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _previewMapButton.titleLabel.font = [UIFont boldSystemFontOfSize:40];
        [_previewMapButton setTitle:@"Przeglądaj mapę" forState:UIControlStateNormal];
        _previewMapButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_buttonsContainer addSubview:_previewMapButton];

        _gameplayButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _gameplayButton.titleLabel.font = [UIFont boldSystemFontOfSize:40];
        [_gameplayButton setTitle:@"Graj" forState:UIControlStateNormal];
        _gameplayButton.enabled = NO;
        _gameplayButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_buttonsContainer addSubview:_gameplayButton];

        _createMapButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _createMapButton.titleLabel.font = [UIFont boldSystemFontOfSize:40];
        [_createMapButton setTitle:@"Stwórz mapę" forState:UIControlStateNormal];
        _createMapButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_buttonsContainer addSubview:_createMapButton];

        _calibrateButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _calibrateButton.titleLabel.font = [UIFont boldSystemFontOfSize:40];
        [_calibrateButton setTitle:@"Kalibruj" forState:UIControlStateNormal];
        _calibrateButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_buttonsContainer addSubview:_calibrateButton];

        _saveMapButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_saveMapButton setTitle:@"Zapisz" forState:UIControlStateNormal];
        _saveMapButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_saveMapButton];

        _downloadMapButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_downloadMapButton setTitle:@"Pobierz" forState:UIControlStateNormal];
        _downloadMapButton.enabled = NO;
        _downloadMapButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_downloadMapButton];

        _uploadMapButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_uploadMapButton setTitle:@"Wyślij" forState:UIControlStateNormal];
        _uploadMapButton.enabled = NO;
        _uploadMapButton.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_uploadMapButton];

        _markersMapSizeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _markersMapSizeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _markersMapSizeLabel.backgroundColor = [UIColor clearColor];
        _markersMapSizeLabel.textColor = [UIColor whiteColor];
        _markersMapSizeLabel.font = [UIFont systemFontOfSize:15];
        [self addSubview:_markersMapSizeLabel];

        NSDictionary *views = @{
                @"self" : self,
                @"camera" : _cameraImage,
                @"buttonsContainer" : _buttonsContainer,
                @"previewMap" : _previewMapButton,
                @"gameplay" : _gameplayButton,
                @"createMap" : _createMapButton,
                @"calibrate" : _calibrateButton,

                @"save" : _saveMapButton,
                @"download" : _downloadMapButton,
                @"upload" : _uploadMapButton,
                @"mapSizeLabel" : _markersMapSizeLabel,


        };

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[camera]|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[camera]|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[gameplay]-[previewMap]-[createMap]-[calibrate]|"
                                                                     options:NSLayoutFormatAlignAllCenterX
                                                                     metrics:nil
                                                                       views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[previewMap]|"
                                                                     options:NSLayoutFormatDirectionLeadingToTrailing
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[mapSizeLabel]-[save]-10-|"
                                                                     options:NSLayoutFormatAlignAllRight
                                                                     metrics:nil
                                                                       views:views]];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[download]-[upload]-[save]-|"
                                                                     options:NSLayoutFormatAlignAllCenterY
                                                                     metrics:nil
                                                                       views:views]];


        [self addConstraints:[NSLayoutConstraint attributeConstraintsWithVisualFormatsArray:@[
                @"self.centerX == buttonsContainer.centerX",
                @"self.centerY == buttonsContainer.centerY",
        ]
                                                                                      views:views]];
    }

    return self;
}

- (void)setMarkersSize:(NSInteger)size {
    NSString *string = size > 0 ? [NSString stringWithFormat:@"Liczba markerów na mapie: %d", size] : @"Brak mapy!";
    _markersMapSizeLabel.text = string;
}

- (void)setNetworkButtonActive:(BOOL)active {
    _downloadMapButton.enabled = active;
    _uploadMapButton.enabled = active;
    _gameplayButton.enabled = active;
}

@end
