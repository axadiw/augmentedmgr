//
//  Created by Michał Mizera on 02.11.2013.
//


#import "AMArEngineDelegate.h"
#import "AMMapPreviewViewController.h"
#import <Objection/Objection.h>
#import "AMArEngine.h"
#import "AMUserDefaultsManager.h"
#import "AMMapPreviewView.h"
#import "UIImage+OpenCV.h"
#import "AMMapPreview3DOverlayView.h"
#import "AMCameraPose.h"
#import "AMMarkersDetector.h"
#import "AMConverter.h"

@interface AMMapPreviewViewController ()
@property(nonatomic, strong) AMArEngine *arEngine;
@property(nonatomic, strong) AMUserDefaultsManager *userDefaultsManager;
@end


@implementation AMMapPreviewViewController {
}

objection_requires_sel(@selector(arEngine), @selector(userDefaultsManager))

- (AMMapPreviewView *)castView {
    return (AMMapPreviewView *) self.view;
}

- (void)loadView {
    self.view = [[AMMapPreviewView alloc] initWithFrame:CGRectZero andMarkersMap:_userDefaultsManager.markersMap];

    [self.castView.backButton addTarget:self action:@selector(backButtonTapped)
                       forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.castView.noMapFoundLabel.hidden = _userDefaultsManager.markersMap != nil;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    _arEngine.mode = AMArEngineModeGameplay;
    [_arEngine addObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    _arEngine.mode = AMArEngineModeDisabled;
    [_arEngine removeObserver:self];
}


- (void)nextFrameReady {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.castView.cameraImage.image = [UIImage imageWithCVMat:*(_arEngine.image)];
        [self.castView setDistance:[AMConverter calculateDistance:_arEngine.cameraPose]];
    });

    if (_arEngine.cameraPose && _arEngine.markers.count > 0) {
        [self.castView.overlay3d updateCameraWithPose:_arEngine.cameraPose];
    }


}


#pragma mark -
#pragma mark Button actions

- (void)backButtonTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
