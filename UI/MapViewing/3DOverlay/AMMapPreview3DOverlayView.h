//
//  Created by Michał Mizera on 13.10.2013.
//


#import <Foundation/Foundation.h>
#import <NinevehGL/NinevehGL.h>

@class AMCameraPose;
@class AMMarkersMap;


@interface AMMapPreview3DOverlayView : NGLView
- (void)setupWithMarkersMap:(AMMarkersMap *)markersMap;

- (void) updateCameraWithPose:(AMCameraPose *)cameraPose;
@end
