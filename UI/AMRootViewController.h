#import <UIKit/UIKit.h>
#import "threeMF.h"
#import "AMArEngineDelegate.h"
#import "AMArEngine.h"
#import "AMMarkersMapViewDelegate.h"

@interface AMRootViewController : UIViewController <AMArEngineDelegate, TMFConnectorDelegate>
@end
