//
//  Created by Michał Mizera on 17.11.2013.
//


#import "MSHelpers.h"


@implementation MSHelpers

+ (CGFloat)distanceBetweenPoints:(CGPoint)pointA pointB:(CGPoint)pointB {
    CGFloat xDistace = (CGFloat) fabs(pointA.x - pointB.x);
    CGFloat yDistance = (CGFloat) fabs(pointA.y - pointB.y);

    return (CGFloat) sqrt(xDistace * xDistace + yDistance * yDistance);
}

@end
