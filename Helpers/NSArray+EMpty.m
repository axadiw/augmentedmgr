//
//  Created by Michał Mizera on 06.11.2013.
//


#import "NSArray+Empty.h"


@implementation NSArray (Empty)

- (BOOL)isEmpty {
    return self.count == 0;
}

@end
