//
//  Created by Michał Mizera on 15.11.2013.
//


#import <Foundation/Foundation.h>

@interface NSView (Center)
- (void)setCenter:(NSPoint)center;
@end
