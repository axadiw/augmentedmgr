//
//  Created by Michał Mizera on 10.11.2013.
//


#import "MSConverter.h"


@implementation MSConverter {

}

+ (NSDictionary *)dictFromPoint:(CGPoint)point {
    return @{@"x" : @(point.x), @"y" : @(-point.y)};
}

+ (CGPoint)cgPointFromDict:(NSDictionary *)dictionary {
    return CGPointMake([dictionary[@"x"] floatValue], -[dictionary[@"y"] floatValue]);
}

@end
