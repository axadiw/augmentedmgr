//
//  Created by Michał Mizera on 13.10.2013.
//


#import <NinevehGL/NinevehGL.h>
#import "AMConverter.h"
#import "cap_ios.h"
#import "AMCameraPose.h"

@implementation AMConverter {

}

+ (NSDictionary *)dictFromPoint2f:(cv::Point2f)point2f {
    return @{@"x" : @(point2f.x), @"y" : @(point2f.y)};
}

+ (cv::Point2f)point2FfromDict:(NSDictionary *)dictionary {
    return cv::Point2f([dictionary[@"x"] floatValue], [dictionary[@"y"] floatValue]);
}

+ (NGLvec3 *)nglVectorFromOpenCV:(cv::Point3f *)vec {
    NGLvec3 *vector = new NGLvec3();

    vector->x = vec->x;
    vector->y = vec->y;
    vector->z = vec->z;

    return vector;
}

+ (NGLvec3 *)translateNGLVectorFromRadiansToDegrees:(NGLvec3 *)vector {
    vector->x = [AMConverter fromRadiansToDegrees:vector->x];
    vector->y = [AMConverter fromRadiansToDegrees:vector->y];
    vector->z = [AMConverter fromRadiansToDegrees:vector->z];

    return vector;
}

+ (NGLvec3 *)translateNGLVectorFromDegreesToRadians:(NGLvec3 *)vector {
    vector->x = [AMConverter fromDegreesToRadians:vector->x];
    vector->y = [AMConverter fromDegreesToRadians:vector->y];
    vector->z = [AMConverter fromDegreesToRadians:vector->z];

    return vector;
}

+ (cv::Point3f *)translateOpenCVVectorFromRadiansToDegrees:(cv::Point3f *)vector {
    vector->x = [AMConverter fromRadiansToDegrees:vector->x];
    vector->y = [AMConverter fromRadiansToDegrees:vector->y];
    vector->z = [AMConverter fromRadiansToDegrees:vector->z];

    return vector;
}

+ (cv::Point3f *)translateOpenCVVectorFromDegreesToRadians:(cv::Point3f *)vector {
    vector->x = [AMConverter fromDegreesToRadians:vector->x];
    vector->y = [AMConverter fromDegreesToRadians:vector->y];
    vector->z = [AMConverter fromDegreesToRadians:vector->z];

    return vector;
}

+ (float)fromRadiansToDegrees:(float)radians {
    return (float) (radians * (180.0f / M_PI));
}

+ (float)fromDegreesToRadians:(float)degrees {
    return (float) (degrees * (M_PI / 180.0f));
}

+ (NSArray *)arrayFromMat:(cv::Mat)mat {
    NSMutableArray *array = [NSMutableArray array];

    for (int x = 0; x < mat.cols; x++) {
        for (int y = 0; y < mat.rows; y++) {
            [array addObject:@(mat.at<double>(x, y))];
        }
    }

    return array;
}

+ (void)nglMatFromOpenCVMat:(cv::Mat)mat nglMat:(NGLmat4 *)output {
    NGLmat4 matrix;
    NSArray *array = [AMConverter arrayFromMat:mat];
    nglMatrixFromNSArray(array, matrix);
    nglMatrixTranspose(matrix, *output);
}


+ (NSString *)strinfFromNGLMatrix:(NGLmat4 *)mat {
    NSMutableString *ret = [NSMutableString string];
    for (int x = 0; x < 4; x++) {
        for (int y = 0; y < 4; y++) {
//            NSLog(@"%d", y * 4 + x);
            [ret appendFormat:@"%.2f ", (*mat)[y * 4 + x]];
        }

        [ret appendString:@"\n"];
    }

    return ret;
}

+ (cv::Mat)translationMatrixWithX:(double)x Y:(double)y Z:(double)z {
    double mat[4][4] = {
            {1, 0, 0, x},
            {0, 1, 0, y},
            {0, 0, 1, z},
            {0, 0, 0, 1},
    };

    return cv::Mat(4, 4, CV_64FC1, (void *) mat).clone();
}

+ (cv::Mat)scaleMatrixWithX:(double)x Y:(double)y Z:(double)z {
    double mat[4][4] = {
            {x, 0, 0, 0},
            {0, y, 0, 0},
            {0, 0, z, 0},
            {0, 0, 0, 1},
    };

    return cv::Mat(4, 4, CV_64FC1, (void *) mat).clone();
}

+ (cv::Mat)rotationMatrixWithX:(double)x Y:(double)y Z:(double)z {
    double matX[4][4] = {
            {1, 0, 0, 0},
            {0, cos(x), -sin(x), 0},
            {0, sin(x), cos(x), 0},
            {0, 0, 0, 1},
    };
    double matY[4][4] = {
            {cos(y), 0, sin(y), 0},
            {0, 1, 0, 0},
            {-sin(y), 0, cos(y), 0},
            {0, 0, 0, 1},
    };
    double matZ[4][4] = {
            {cos(z), -sin(z), 0, 0},
            {sin(z), cos(z), 0, 0},
            {0, 0, 1, 0},
            {0, 0, 0, 1},
    };

    cv::Mat X = cv::Mat(4, 4, CV_64FC1, (void *) matX).clone();
    cv::Mat Y = cv::Mat(4, 4, CV_64FC1, (void *) matY).clone();
    cv::Mat Z = cv::Mat(4, 4, CV_64FC1, (void *) matZ).clone();

    return X * Y * Z;
}


+ (cv::Mat)rotationMatrix33WithX:(double)x Y:(double)y Z:(double)z {
    double matX[3][3] = {
            {1, 0, 0},
            {0, cos(x), -sin(x)},
            {0, sin(x), cos(x)},
    };
    double matY[3][3] = {
            {cos(y), 0, sin(y)},
            {0, 1, 0},
            {-sin(y), 0, cos(y)},
    };
    double matZ[3][3] = {
            {cos(z), -sin(z), 0},
            {sin(z), cos(z), 0},
            {0, 0, 1},
    };

    cv::Mat X = cv::Mat(3, 3, CV_64FC1, (void *) matX).clone();
    cv::Mat Y = cv::Mat(3, 3, CV_64FC1, (void *) matY).clone();
    cv::Mat Z = cv::Mat(3, 3, CV_64FC1, (void *) matZ).clone();

    return X * Y * Z;
}


+ (CGFloat)calculateDistance:(AMCameraPose *)pose {
    if(pose.tvecInv.cols <= 0 || pose.tvecInv.rows <= 0) return -1;

    double x = pose.tvecInv.at<double>(0, 0);
    double y = pose.tvecInv.at<double>(1, 0);

    CGFloat normalizedDistance = sqrt(pow(x, 2) + pow(y, 2));
    return (normalizedDistance) / 100.0f;
}

@end
