//
//  Created by Michał Mizera on 06.11.2013.
//


#import "NSDictionary+Empty.h"


@implementation NSDictionary (Empty)

- (BOOL)isEmpty {
    return self.count == 0;
}


@end
