
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Objection
#define COCOAPODS_POD_AVAILABLE_Objection
#define COCOAPODS_VERSION_MAJOR_Objection 1
#define COCOAPODS_VERSION_MINOR_Objection 2
#define COCOAPODS_VERSION_PATCH_Objection 0

// iOS-Hierarchy-Viewer
#define COCOAPODS_POD_AVAILABLE_iOS_Hierarchy_Viewer
#define COCOAPODS_VERSION_MAJOR_iOS_Hierarchy_Viewer 1
#define COCOAPODS_VERSION_MINOR_iOS_Hierarchy_Viewer 4
#define COCOAPODS_VERSION_PATCH_iOS_Hierarchy_Viewer 7

