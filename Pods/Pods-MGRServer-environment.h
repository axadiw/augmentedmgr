
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// box2d
#define COCOAPODS_POD_AVAILABLE_box2d
#define COCOAPODS_VERSION_MAJOR_box2d 2
#define COCOAPODS_VERSION_MINOR_box2d 3
#define COCOAPODS_VERSION_PATCH_box2d 0

// threeMF
#define COCOAPODS_POD_AVAILABLE_threeMF
#define COCOAPODS_VERSION_MAJOR_threeMF 0
#define COCOAPODS_VERSION_MINOR_threeMF 1
#define COCOAPODS_VERSION_PATCH_threeMF 0

