//
//  main.m
//  OpenCVClient
//
//  Created by Robin Summerhill on 02/09/2011.
//  Copyright 2011 Aptogo Limited. All rights reserved.
//
//  Permission is given to use this source code file without charge in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import <UIKit/UIKit.h>
#import "AMAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, NSStringFromClass([AMAppDelegate class]));
        return retVal;
    }
}

// From here to end of file added by Injection Plugin //
//
//#ifdef DEBUG
//#define INJECTION_PORT 31444 // AppCode
//static char _inMainFilePath[] = __FILE__;
//static const char *_inIPAddresses[] = {"192.168.1.104", NULL};
//
//#define INJECTION_ENABLED
//#import "/Users/michalmizera/Library/Application Support/Developer/Shared/Xcode/Plug-ins/InjectionPlugin.xcplugin/Contents/Resources//BundleInjection.h"
//#endif
