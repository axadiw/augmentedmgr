//
//  Created by Michał Mizera on 16.11.2013.
//


#import <Foundation/Foundation.h>
#import "TMFAnnounceCommand.h"

@interface AMStartGameCommandArguments : TMFAnnounceCommandArguments
@property(nonatomic) CGFloat x;
@property(nonatomic) CGFloat y;
@end
