//
//  Created by Michał Mizera on 16.11.2013.
//


#import "AMDownloadGameStateCommand.h"


NSString *MSPhysicObjectIdIdentifier = @"i";
NSString *MSPhysicObjectTypeIdentifier = @"t";
NSString *MSPhysicObjectXPosIdentifier = @"x";
NSString *MSPhysicObjectYPosIdentifier = @"y";
NSString *MSPhysicObjectAngleIdentifier = @"a";

@implementation AMDownloadGameStateCommand {

}

+ (NSString *)name {
    return @"download_gamestate";
}

@end
