//
//  Created by Maciej Oczko on 7/17/13.
//


#import "AMObjectionModule.h"
#import "AMArEngine.h"
#import "AMUserDefaultsManager.h"
#import "AMCalibrator.h"
#import "AMMarkersDetector.h"

@implementation AMObjectionModule {

}

+ (instancetype)module {
    return [[self alloc] init];
}

- (void)configure {
    [self bindClass:[AMArEngine class] inScope:JSObjectionScopeSingleton];
    [self bindClass:[AMCalibrator class] inScope:JSObjectionScopeSingleton];
    [self bindClass:[AMUserDefaultsManager class] inScope:JSObjectionScopeSingleton];
}

@end
