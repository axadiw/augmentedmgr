//
//  Created by Maciej Oczko on 7/17/13.
//


#import <Foundation/Foundation.h>
#import "JSObjectionModule.h"

@interface AMObjectionModule : JSObjectionModule
+ (instancetype)module;
@end
