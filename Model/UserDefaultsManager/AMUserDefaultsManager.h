//
//  Created by Michał Mizera on 31.07.13.
//

#import <Foundation/Foundation.h>

@class AMMarkersMap;

@interface AMUserDefaultsManager : NSObject

@property(nonatomic) cv::Mat distCoeffs;

@property(nonatomic, readonly) NSString * videoQualityPreset;

@property(nonatomic) AMMarkersMap * markersMap;

- (cv::Mat)cameraMatrixWithCurrentResolution:(CGSize)size;

- (CGFloat)markerSize;

- (void)setCameraMatrix:(cv::Mat)cameraMatrix withResolution:(CGSize)size;
- (void)clearMarkers;
- (void)saveMarkerMap;
@end
