//
//  Created by Michał Mizera on 03.11.2013.
//


#import <Foundation/Foundation.h>


using namespace cv;

@interface AMMarkersDetector : NSObject

@property(nonatomic) NSInteger lastError;

@property(nonatomic, strong) NSMutableArray *markers;

@property(nonatomic) Mat *undistortedImage;

- (id)initWithImageSize:(NSValue *)size;

- (void)currentMarkersInImage:(Mat *)image;

- (void)markersInImage:(Mat *)image;
@end
