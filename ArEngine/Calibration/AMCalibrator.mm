//
//  Created by Michał Mizera on 05.10.2013.
//


#import <Objection/Objection.h>
#import "AMCalibrator.h"
#import "AMUserDefaultsManager.h"
#import "UIImage+OpenCV.h"

@interface AMCalibrator ()
@property(nonatomic, strong) AMUserDefaultsManager *userDefaultsManager;
@end

@implementation AMCalibrator {
    std::vector< std::vector<cv::Point2f> > *_potentiallyUsedCorners;
    CvSize _patternSize;
    CvSize _imageSize;
    NSMutableArray *_rawImagePaths;
    NSMutableArray *_processedImagePaths;
}

objection_requires_sel(@selector(userDefaultsManager))


- (id)init {
    self = [super init];
    if (self) {
        _patternSize = cvSize(6, 9);
    }

    return self;
}

- (void)startCalibration {
    _potentiallyUsedCorners = new std::vector< std::vector< cv::Point2f> >();
    _approvedCorners = new std::vector< std::vector< cv::Point2f> >();
    _rawImagePaths = [NSMutableArray array];
    _processedImagePaths = [NSMutableArray array];
    _lastPresentedProcessedImage = 0;
}

- (void)endCalibration {
    NSLog(@"Zebrano %lu obrazków", _approvedCorners->size());

    if (_approvedCorners->empty()) {
        return;
    }

    NSDate *startDate = [NSDate date];

    std::vector< std::vector<cv::Point3f> > *objectPoints = new std::vector< std::vector< cv::Point3f> >();

    for (unsigned long i = 0; i < _approvedCorners->size(); i++) {
        std::vector<cv::Point2f> currentImagePoints = _approvedCorners->at(i);
        std::vector<cv::Point3f> currentObjectPoints;

        for (int j = 0; j < currentImagePoints.size(); j++) {
            cv::Point3f newPoint = cv::Point3f(j % _patternSize.width, j / _patternSize.width, 0);

            currentObjectPoints.push_back(newPoint);
        }

        objectPoints->push_back(currentObjectPoints);
    }

    std::vector<cv::Mat> rvecs, tvecs;

    static CGSize size = CGSizeMake(_imageSize.width, _imageSize.height);
    cv::Mat cameraMatrix = [_userDefaultsManager cameraMatrixWithCurrentResolution:size];
    cv::Mat coeffs = _userDefaultsManager.distCoeffs;
    cv::calibrateCamera(*objectPoints, *_approvedCorners, _imageSize, cameraMatrix, coeffs, rvecs, tvecs);


    _userDefaultsManager.distCoeffs = coeffs;
    [_userDefaultsManager setCameraMatrix:cameraMatrix withResolution:size];

    NSTimeInterval executionTime = [[NSDate date] timeIntervalSinceDate:startDate];
    NSLog(@"Czas trwania kalibracji: %f sekund", executionTime);
}

- (void)saveRawImage:(UIImage *)image {
    NSString *imageId = [NSString stringWithFormat:@"raw%d", [_rawImagePaths count]];
    NSString *path = [self saveImage:image withname:imageId];
    [_rawImagePaths addObject:path];
}

- (void)saveProcessedImage:(UIImage *)image {
    NSString *imageId = [NSString stringWithFormat:@"processed%d", [_processedImagePaths count]];
    NSString *path = [self saveImage:image withname:imageId];
    [_processedImagePaths addObject:path];
}

- (NSString *)saveImage:(UIImage *)image withname:(NSString *)name {
    NSString *str = [NSString stringWithFormat:@"Documents/%@.png", name];
    NSString *pngPath = [NSHomeDirectory() stringByAppendingPathComponent:str];

    [UIImagePNGRepresentation(image) writeToFile:pngPath atomically:YES];

    return pngPath;
}


- (UIImage *)loadImageFromPath:(NSString *)path {
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    return image;
}

- (void)processSavedImages {

    NSInteger index = 1;
    UIImage *image;
    for (NSString *imagePath in _rawImagePaths) {
        image = [UIImage imageWithContentsOfFile:imagePath];
        std::vector<cv::Point2f> corners;

        Mat imageMat = [image CVMat];
        _imageSize = cvSize(imageMat.size().width, imageMat.size().height);

        bool found = cv::findChessboardCorners(imageMat, _patternSize, corners);

        if (found) {
            cv::Mat *gray_image = new cv::Mat(imageMat.size().height, imageMat.size().width, CV_8UC1);
            cv::cvtColor(imageMat, *gray_image, CV_RGB2GRAY);

            cv::cornerSubPix(*gray_image, corners, cvSize(11, 11), cvSize(-1, -1), cvTermCriteria(CV_TERMCRIT_EPS+ CV_TERMCRIT_ITER, 30, 0.1));

            cv::drawChessboardCorners(imageMat, _patternSize, corners, found);

            _potentiallyUsedCorners->push_back(corners);
            UIImage *processedImage = [[UIImage alloc] initWithCVMat:imageMat];
            [self saveProcessedImage:processedImage];
        }

        if ([_delegate respondsToSelector:@selector(updateProgressBarWithValue:)]) {
            [_delegate updateProgressBarWithValue:(CGFloat) index / (CGFloat) [_rawImagePaths count]];
        }

        index++;
    }
}

- (UIImage *)loadNextProcessedImage {
    if (_processedImagePaths.count > _lastPresentedProcessedImage) {
        NSString *path = _processedImagePaths[_lastPresentedProcessedImage++];

        if ([_delegate respondsToSelector:@selector(updateProgressBarWithValue:)]) {
            [_delegate updateProgressBarWithValue:(CGFloat) _lastPresentedProcessedImage / (CGFloat) [_processedImagePaths count]];
        }

        return [self loadImageFromPath:path];
    }

    return nil;
}

- (void)acceptCurrentProcessedImage {
    std::vector<cv::Point2f> corners = _potentiallyUsedCorners->at(_lastPresentedProcessedImage - 1);
    _approvedCorners->push_back(corners);
}

@end
