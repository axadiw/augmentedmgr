//
//  Created by Michał Mizera on 09.11.2013.
//


#import "AMMarkersMap.h"
#import "AMMarker.h"
#import "NSDictionary+Empty.h"


@implementation AMMarkersMap {

}

- (instancetype)init {
    self = [super init];
    if (self) {
        _markersMap = [NSMutableDictionary dictionary];
    }

    return self;
}


- (NSString *)serializedMap {
    NSArray *markers = _markersMap.allValues;
    NSMutableArray *serializedMarkers = [NSMutableArray array];

    for (AMMarker *marker in markers) {
        NSString *json = [marker jsonRepresentation];

        [serializedMarkers addObject:json];
    }

    NSData *serializedMapData = [NSJSONSerialization dataWithJSONObject:serializedMarkers
                                                                options:nil
                                                                  error:nil];
    NSString *serializedMap = [[NSString alloc] initWithData:serializedMapData
                                                    encoding:NSUTF8StringEncoding];

    return serializedMap;
}


- (void)loadMap:(NSString *)serializedMap {
    if (serializedMap) {
        NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:[serializedMap dataUsingEncoding:NSUTF8StringEncoding]
                                                              options:nil
                                                                error:nil];
        for (NSString *serializedMarker in jsonObject) {
            NSDictionary *markerDict = [NSJSONSerialization JSONObjectWithData:[serializedMarker dataUsingEncoding:NSUTF8StringEncoding]
                                                                       options:nil
                                                                         error:nil];
            AMMarker *marker = [[AMMarker alloc] initWithDict:markerDict];
            _markersMap[@(marker.id)] = marker;
        }
    }
}

- (NSInteger)count {
    return _markersMap.count;
}

- (id)objectForKeyedSubscript:(id)key {
    return [_markersMap objectForKeyedSubscript:key];
}

- (NSArray *)allValues {
    return _markersMap.allValues;
}

- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key {
    [_markersMap setObject:obj forKeyedSubscript:key];
}

- (BOOL)isEmpty {
    return [_markersMap isEmpty];
}
@end
