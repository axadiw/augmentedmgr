//
//  Created by Michał Mizera on 09.11.2013.
//


#import <Foundation/Foundation.h>


@interface AMMarkersMap : NSObject

@property (nonatomic) NSMutableDictionary * markersMap;

- (NSString *)serializedMap;

- (void)loadMap:(NSString *)serializedMap;

- (NSInteger)count;

- (id)objectForKeyedSubscript:(id)key;

- (NSArray *)allValues;

- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key;

- (BOOL)isEmpty;
@end
