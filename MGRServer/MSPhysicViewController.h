//
//  MSPhysicViewController.h
//  MGRServer
//
//  Created by Michal Mizera on 14.11.2013.
//  Copyright (c) 2013 Michal Mizera. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MSPhysicView.h"
#import "AMDownloadGameStateCommand.h"

@class AMDownloadGameStateCommand;
@class TMFPeer;

extern CGFloat PHYSIC_SCALE;

@interface MSPhysicViewController : NSViewController <MSPhysicViewDelegate>

@property(nonatomic, strong) AMDownloadGameStateCommand *gameStateUpdateCommand;

- (void)addPlayer:(TMFPeer *)peer AtPosition:(NSPoint)point;

- (void)movePlayer:(TMFPeer *)peer withPosition:(NSPoint)position;

- (void)reset;
@end
