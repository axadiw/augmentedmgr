//
//  Created by Michał Mizera on 17.04.2013.
//


#import "MSMarker.h"
#import "MSConverter.h"

@implementation MSMarker
- (id)copyWithZone:(NSZone *)zone {
    MSMarker *newMarker = [[MSMarker allocWithZone:zone] init];
    if (newMarker) {
        newMarker.id = self.id;
        newMarker.center = self.center;
        newMarker.lastVisible = self.lastVisible;
        newMarker.size = self.size;
        newMarker.rotation = self.rotation;
        newMarker.fixPosition = self.fixPosition;
    }

    return newMarker;
}

- (id)init {
    self = [super init];
    if (self) {
        _fixPosition = NO;
    }

    return self;
}

- (NSString *)markerDescription {
    NSString *description = [NSString stringWithFormat:@"Id %ld Center: %f, %f  rotation: %f size: %f", (long)_id,
                                                       _center.x, _center.y,
                                                       _rotation * 180.0f / M_PI, _size];
    return description;
}

- (NSString *)debugDescription {
    return [self markerDescription];
}

#pragma mark -
#pragma mark Serialization

- (NSDictionary *)dictRepresentation {
    NSMutableArray *imagePoints = [NSMutableArray array];

    NSDictionary *center = [MSConverter dictFromPoint:_center];

    return @{
            @"id" : @(_id),
            @"center" : center,
            @"rotation" : @(_rotation),
            @"size" : @(_size),
            @"imagePoints" : imagePoints,
            @"lastVisible" : @(_lastVisible),
            @"fixPosition" : @(_fixPosition),
    };
}

- (NSString *)jsonRepresentation {
    NSData *serializedData = [NSJSONSerialization dataWithJSONObject:[self dictRepresentation]
                                                             options:nil
                                                               error:nil];
    NSString *serialized = [[NSString alloc] initWithData:serializedData encoding:NSUTF8StringEncoding];

    return serialized;
}

- (id)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _id = [dict[@"id"] intValue];
        _center = [MSConverter cgPointFromDict:dict[@"center"]];
        _rotation = [dict[@"rotation"] floatValue];
        _size = [dict[@"size"] floatValue];
        _lastVisible = [dict[@"lastVisible"] doubleValue];
        _fixPosition = [dict[@"fixPosition"] boolValue];
    }

    return self;
}


@end
