//
//  Created by Michał Mizera on 11.11.2013.
//


#import <GLKit/GLKit.h>
#import "MSMarkerMapView.h"
#import "MSMarkersMap.h"
#import "MSMarker.h"
#import "MSMarkerView.h"
#import "NSView+Center.h"

NSInteger kMarkerViewSize = 25;

@implementation MSMarkerMapView {
    MSMarkersMap *_markersMap;
    NSMutableDictionary *_markersViewMap;
}

- (void)setupMarkersViewsWithMap:(MSMarkersMap *)aMarkersMap {
    _markersMap = aMarkersMap;
    _markersViewMap = [NSMutableDictionary dictionary];

    [self removeConstraints:self.constraints];

    while ([[self subviews] count] > 0) {
        [[[self subviews] objectAtIndex:0] removeFromSuperview];
    }

    for (MSMarker *marker in aMarkersMap.allValues) {
        MSMarkerView *markerView = [self markerViewWithMarker:marker];
        [self addSubview:markerView];

        _markersViewMap[@(marker.id)] = markerView;
    }

    [self setNeedsLayout:YES];
}

- (MSMarkerView *)markerViewWithMarker:(MSMarker *)marker {
    MSMarkerView *markerView = [[MSMarkerView alloc] initWithFrame:CGRectMake(0, 0, kMarkerViewSize, kMarkerViewSize)
                                                          markerId:marker.id];

    return markerView;
}

- (void)resizeSubviewsWithOldSize:(NSSize)oldSize {
    CGPoint center = CGPointMake(NSWidth(self.frame) / 2.0f, NSHeight(self.frame) / 2.0f);

    for (MSMarker *marker in _markersMap.allValues) {
        MSMarkerView *markerView = _markersViewMap[@(marker.id)];

        NSPoint point = NSMakePoint(center.x + marker.center.x * kMarkerViewSize, center.y + marker.center.y * kMarkerViewSize);

        [markerView setFrameCenterRotation:0];
        [markerView setCenter:point];
        [markerView setFrameSize:NSMakeSize(kMarkerViewSize, kMarkerViewSize)];
        [markerView setFrameCenterRotation:GLKMathRadiansToDegrees((float) -marker.rotation)];

    }

    [super resizeSubviewsWithOldSize:oldSize];
}

@end
