//
//  Created by Michał Mizera on 11.11.2013.
//


#import <Foundation/Foundation.h>


@interface MSMarkerView : NSView
- (id)initWithFrame:(NSRect)frameRect markerId:(NSInteger)markerId;

@end
