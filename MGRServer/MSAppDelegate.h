//
//  MSAppDelegate.h
//  MGRServer
//
//  Created by Michal Mizera on 09.11.2013.
//  Copyright (c) 2013 Michal Mizera. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MSMasterViewController.h"

@interface MSAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (nonatomic,strong) IBOutlet MSMasterViewController *masterViewController;

@end
