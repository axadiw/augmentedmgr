//
//  main.m
//  MGRServer
//
//  Created by Michal Mizera on 09.11.2013.
//  Copyright (c) 2013 Michal Mizera. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
