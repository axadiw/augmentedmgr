//
//  Created by Michał Mizera on 15.11.2013.
//


#import <box2d/Dynamics/b2World.h>
#import <box2d/Collision/Shapes/b2PolygonShape.h>
#import <box2d/Dynamics/b2Body.h>
#import <box2d/Dynamics/b2Fixture.h>

#ifndef __QueryCallback_H_
#define __QueryCallback_H_

class QueryCallback : public b2QueryCallback {
public:
    QueryCallback(const b2Vec2 &point) {
        m_point = point;
        m_fixture = NULL;
    }

    bool ReportFixture(b2Fixture *fixture) {
        bool inside = fixture->TestPoint(m_point);
        if (inside) {
            m_fixture = fixture;

            // We are done, terminate the query.
            return false;
        }

        // Continue the query.
        return true;
    }

    b2Vec2 m_point;
    b2Fixture *m_fixture;
};


#endif //__QueryCallback_H_
