//
//  Created by Michał Mizera on 15.11.2013.
//


#import <Foundation/Foundation.h>

@protocol MSPhysicViewDelegate <NSObject>
- (void)keyDown:(NSString *)chars mousePosition:(NSPoint)mousePosition;
@end


@interface MSPhysicView : NSView

@property (nonatomic) id<MSPhysicViewDelegate> delegate;

@end
