//
//  MSPhysicViewController.m
//  MGRServer
//
//  Created by Michal Mizera on 14.11.2013.
//  Copyright (c) 2013 Michal Mizera. All rights reserved.
//

#import <box2d/Dynamics/b2World.h>
#import <GLKit/GLKit.h>
#import <threeMF/TMFKeyValueCommand.h>
#import <threeMF/TMFPeer.h>
#import "MSPhysicViewController.h"
#import "MSPhysicObject.h"
#import "NSView+Center.h"
#import "QueryCallback.h"
#import "AMDownloadGameStateCommandArguments.h"
#import "MSMarkerMapView.h"

CGFloat PHYSIC_SCALE = 1.0f;
float32 physicDeltaTime = 1.0f / 30.0f;
CGFloat publishDeltaTime = 1.0f / 10.0f;
NSInteger kCarsAmount = 4;
NSString *kPhysicObjectToAddTypeIdentifier = @"type";
NSString *kPhysicObjectToAddPositionIdentifier = @"pos";
NSString *kPhysicObjectToAddPlayerIdentifier = @"player";

@interface MSPhysicViewController ()

@end

@implementation MSPhysicViewController {
    b2World *_world;
    NSMutableArray *_physicObjects;
    NSMutableDictionary *_playersCars;

    NSMutableSet *_physicObjectsToDelete;
    NSMutableArray *_physicObjectsToAdd;
    dispatch_queue_t _queue;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        _world = new b2World(b2Vec2::b2Vec2(0.0f, 0.0f));
        _physicObjects = [NSMutableArray array];
        _physicObjectsToDelete = [NSMutableSet set];
        _physicObjectsToAdd = [NSMutableArray array];
        _playersCars = [NSMutableDictionary dictionary];

        _queue = dispatch_queue_create("my queue", DISPATCH_QUEUE_PRIORITY_DEFAULT);
        [self update];
    }

    return self;
}

- (void)loadView {
    MSPhysicView *physicView = [[MSPhysicView alloc] initWithFrame:CGRectZero];
    self.view = physicView;

    physicView.delegate = self;
}

- (void)update {
    @synchronized (self) {
        _world->Step(physicDeltaTime, 2, 6);

        [self updatePhysicObjects];
        [self addNecessaryObjects];
        [self deleteNecessaryPhysicObjects];
        [self publishChanges];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (physicDeltaTime * NSEC_PER_SEC)), _queue, ^{
            [self update];
        });
    }
}

- (void)publishChanges {
    @synchronized (self) {
        static NSDate *lastPublishDate = [NSDate date];
        NSTimeInterval dt = -[lastPublishDate timeIntervalSinceNow];
        if (dt < publishDeltaTime) {
            return;
        }
        lastPublishDate = [NSDate date];

        NSMutableArray *currentState = [NSMutableArray arrayWithCapacity:_physicObjects.count];

        for (MSPhysicObject *physicObject in _physicObjects) {
            [currentState addObject:[physicObject dictRepresentation]];
        }
        NSData *serializedData = [NSJSONSerialization dataWithJSONObject:currentState
                                                                 options:nil
                                                                   error:nil];
        NSString *serializedState = [[NSString alloc] initWithData:serializedData encoding:NSUTF8StringEncoding];

        AMDownloadGameStateCommandArguments *arguments = [AMDownloadGameStateCommandArguments new];
        arguments.key = @"s";
        arguments.value = serializedState;
        [_gameStateUpdateCommand sendWithArguments:arguments];
    }
}


- (void)updatePhysicObjects {
    dispatch_async(dispatch_get_main_queue(), ^{
        @synchronized (self) {
            for (MSPhysicObject *physicObject in _physicObjects) {
                b2Vec2 position = physicObject.body->GetPosition();

                CGPoint center = CGPointMake(position.x * kMarkerViewSize * PHYSIC_SCALE, position.y * kMarkerViewSize * PHYSIC_SCALE);
                center.x += NSWidth(self.view.frame) / 2.0f;
                center.y += NSHeight(self.view.frame) / 2.0f;

                [physicObject setFrameCenterRotation:0];
                [physicObject setCenter:center];
                [physicObject setFrameCenterRotation:GLKMathRadiansToDegrees(physicObject.body->GetAngle())];
            }
        };
    });
}

#pragma mark -
#pragma mark MSPhysicViewDelegate

- (void)keyDown:(NSString *)characters mousePosition:(NSPoint)mousePosition {
    @synchronized (self) {


        MSPhysicObjectType type;

        if ([[characters lowercaseString] isEqualToString:@"x"]) {
            [self deletePhysicObjectAtPoint:mousePosition];
            return;
        } else if ([characters isEqualToString:@"1"]) {
            type = MSPhysicObjectTypeRock;
        } else if ([characters isEqualToString:@"2"]) {
            type = MSPhysicObjectTypeBox;
        } else if ([characters isEqualToString:@"3"]) {
            type = MSPhysicObjectTypeTree;
        } else if ([characters isEqualToString:@"4"]) {
            type = MSPhysicObjectTypeCone;
        } else {
            return;
        }

        [_physicObjectsToAdd addObject:@{kPhysicObjectToAddTypeIdentifier : @((type)), kPhysicObjectToAddPositionIdentifier : [NSValue valueWithPoint:mousePosition]}];
    }
}

#pragma mark -
#pragma mark Adding Objects


- (void)addNecessaryObjects {
    dispatch_async(dispatch_get_main_queue(), ^{
        @synchronized (self) {
            for (NSDictionary *physicObjectToAdd in _physicObjectsToAdd) {
                MSPhysicObjectType type = (MSPhysicObjectType) [physicObjectToAdd[kPhysicObjectToAddTypeIdentifier] intValue];
                NSPoint position = [physicObjectToAdd[kPhysicObjectToAddPositionIdentifier] pointValue];
                MSPhysicObject *newObject = [self executeAddPhysicObjectAction:type atPosition:position];

                NSString *playerId = physicObjectToAdd[kPhysicObjectToAddPlayerIdentifier];
                if (playerId) {
                    _playersCars[playerId] = newObject;
                }
            }

            [_physicObjectsToAdd removeAllObjects];
        }
    });
}

- (MSPhysicObject *)executeAddPhysicObjectAction:(MSPhysicObjectType)type atPosition:(NSPoint)objectPosition {
    @synchronized (self) {
        MSPhysicObject *object = [MSPhysicObject physicObjectWithType:type
                                                             position:objectPosition
                                                                world:_world];
        [self.view addSubview:object];
        [_physicObjects addObject:object];

        return object;
    };
}

#pragma mark -
#pragma mark deleting objects

- (void)deleteNecessaryPhysicObjects {
    dispatch_async(dispatch_get_main_queue(), ^{
        @synchronized (self) {
            for (MSPhysicObject *physicObject in _physicObjectsToDelete) {
                [physicObject removeFromSuperview];
                _world->DestroyBody(physicObject.body);
                [_physicObjects removeObject:physicObject];
            }

            [_physicObjectsToDelete removeAllObjects];
        }
    });
}

- (void)deletePhysicObjectAtPoint:(NSPoint)mousePosition {
    @synchronized (self) {
        b2Vec2 pos = b2Vec2((float32) (mousePosition.x / (PHYSIC_SCALE * kMarkerViewSize)), (float32) (mousePosition.y / (PHYSIC_SCALE * kMarkerViewSize)));
        b2AABB aabb;
        b2Vec2 d;
        d.Set(0.001f, 0.001f);
        aabb.lowerBound = pos - d;
        aabb.upperBound = pos + d;

        // Query the world for overlapping shapes.
        QueryCallback callback(pos);

        _world->QueryAABB(&callback, aabb);

        if (callback.m_fixture) {

            for (MSPhysicObject *physicObject in _physicObjects) {
                BOOL objectIsACar = [_playersCars.allValues doesContain:physicObject];
                if (physicObject.fixture == callback.m_fixture && !objectIsACar) {
                    [_physicObjectsToDelete addObject:physicObject];
                    return;
                }
            }
        }
    };
}


- (void)addPlayer:(TMFPeer *)peer AtPosition:(NSPoint)point {
    if (!_playersCars[peer.name]) {

        point.x *= kMarkerViewSize;
        point.y *= kMarkerViewSize;

        NSInteger typeId = [_playersCars count] % kCarsAmount;
        NSInteger type;

        switch (typeId) {
            case 0:
                type = MSPhysicObjectTypeCar1;
                break;
            case 1:
                type = MSPhysicObjectTypeCar2;
                break;
            case 2:
                type = MSPhysicObjectTypeCar3;
                break;
            case 3:
                type = MSPhysicObjectTypeCar4;
                break;
            default:
                type = MSPhysicObjectTypeCar4;
                break;
        }


        [_physicObjectsToAdd addObject:@{
                kPhysicObjectToAddTypeIdentifier : @((type)),
                kPhysicObjectToAddPositionIdentifier : [NSValue valueWithPoint:point],
                kPhysicObjectToAddPlayerIdentifier : peer.name
        }];
    }
}

- (void)movePlayer:(TMFPeer *)peer withPosition:(NSPoint)position {
    @synchronized (self) {
        MSPhysicObject *car = _playersCars[[peer name]];
        if (car) {

            b2Vec2 direction = car.body->GetTransform().q.GetYAxis();
            direction *= (position.y / (PHYSIC_SCALE)) * 40.0f;
            car.body->ApplyForce(direction, car.body->GetPosition());


            car.body->ApplyAngularImpulse((float32) (-position.x / (25.0f * PHYSIC_SCALE)));
            float32 angularVelocity = car.body->GetAngularVelocity();
            angularVelocity = MAX(angularVelocity, -4);
            angularVelocity = MIN(angularVelocity, 4);

            car.body->SetAngularVelocity(angularVelocity);
        }
    }
}

- (void)reset {
    @synchronized (self) {
        for (MSPhysicObject *physicObject in _physicObjects) {
            BOOL objectIsACar = [_playersCars.allValues doesContain:physicObject];
            if (!objectIsACar) {
                [_physicObjectsToDelete addObject:physicObject];
            }
        }

        for (MSPhysicObject *car in _playersCars.allValues) {
            car.body->SetTransform(b2Vec2(0, 0), 0);
        }
    }
}
@end
