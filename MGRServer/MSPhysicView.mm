//
//  Created by Michał Mizera on 15.11.2013.
//


#import "MSPhysicView.h"


@implementation MSPhysicView {

}

- (BOOL)acceptsFirstResponder {
    return YES;
}

- (void)keyDown:(NSEvent *)theEvent {
    NSPoint globalLocation = [NSEvent mouseLocation];
    NSPoint windowLocation = [[self window] convertScreenToBase:globalLocation];
    NSPoint viewLocation = [self convertPoint:windowLocation fromView:nil];

    viewLocation.x -= NSWidth(self.frame) / 2.0f;
    viewLocation.y -= NSHeight(self.frame) / 2.0f;

    if ([_delegate respondsToSelector:@selector(keyDown:mousePosition:)]) {
        [_delegate keyDown:theEvent.characters mousePosition:viewLocation];
    }
}

@end
